class Attend < ActiveRecord::Base

	# model attend meeting_id:integer name:string{32} msisdn:string{10} initiator:string{2} accept:string{3} cancelled:string{4}

	def self.insert_attendees(code, i_name, i_msisdn, count, attendees)
		begin
			Attend.create(:meeting_id => code, 
					:name => i_name, 
					:msisdn => i_msisdn, 
					:initiator => "1",
					:accept => "1", 
					:cancelled => "no"
					);
			puts "new attendee created - initiator"
			(1..count).each do |i|
				a = 'name'+i.to_s
				b = 'msisdn'+i.to_s
				Attend.create(:meeting_id => code, 
					:name => attendees[a], 
					:msisdn => attendees[b], 
					:initiator => "0",
					:accept => "-1", 
					:cancelled => "no"
					);
				puts "new attendee created"
			end
			return 1
		rescue Exception => exec
			logger.error("ERROR: #{exec.message}")
			return 0
		end
	end


	def self.check_meeting_requests(name, msisdn)
		resultset = Attend.select(:meeting_id).where("name = ? AND msisdn = ? AND initiator=? AND accept =? AND cancelled =?", name, msisdn, "0", "-1", "no");
		count = 0;
		response = Hash.new;
		if !resultset.empty?
			resultset.each do |result|
				count=count + 1;
				a= count.to_i;
				response[a] = result.meeting_id;
			end
			response[:count] = count;
		else
			response[:count] = count;
		end
		return response;
	end

	def self.check_my_meetings(name, msisdn)
		resultset = Attend.select(:meeting_id).where("name = ? AND msisdn = ? AND accept =? AND cancelled =?", name, msisdn,"1", "no");
		count = 0;
		response = Hash.new;
		if !resultset.empty?
			resultset.each do |result|
				count=count + 1;
				a= count.to_i;
				response[a] = result.meeting_id;
			end
			response[:count] = count;
		else
			response[:count] = count;
		end
		return response;
	end
end
