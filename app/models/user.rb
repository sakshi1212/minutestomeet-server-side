class User < ActiveRecord::Base

	def self.insertnew(name, email, password, msisdn)
		begin
			User.create(:email => email, 
					:password => password, 
					:name => name, 
					:msisdn => msisdn,
					:imei => "xxx", 
					:dp_url => "xxx");
			puts "new user created"
			return 1
		rescue Exception => exec
			logger.error("ERROR: #{exec.message}")
			return 0
		end
	end

	def self.insertdebug
		User.create(:email => "sakshi@xyz.com", :password => "asdf", :name => 'Sakshi', :imei => 'xxx', :dp_url => "tobeset");
	end

	def self.check_user(email, password)
		response = {};
		resultset = User.select(:name, :password, :msisdn).where("email =?", email);
		if !resultset.empty?
			resultset.each do |result|
				response[:name] = result.name
				response[:msisdn] = result.msisdn
				if (result.password.eql? password)
					response[:login] = "success";
					response[:code] = '1';
				else 
					response[:login] = "not success";
					response[:code] = '2';
				end
			end
		else
			response[:name] = "no"
			response[:msisdn] = "no"
			response[:login] = "not success";
			response[:code] = '3';
		end
		puts response.inspect
		return response;
	end
end
