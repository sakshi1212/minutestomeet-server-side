class LoginController < ApplicationController

	def req_for_login
		puts 'Checking table users if the user exists and verifying email and password';
		email = params[:email];
		password = params[:password];
		response = User.check_user(email, password);
		render :json => response;
	end

end
