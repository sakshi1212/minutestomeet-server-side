class MeetingController < ApplicationController
	def new_meeting
		# Parameters: {"title"=>"bbbbb", "day"=>"13", "month"=>"12", "year"=>"2015", "hour"=>"16", "minute"=>"00", "count"=>"1", "name1"=>"A P", "msisdn1"=>"+919742552649", "latitude"=>"22.32159397452096", "longitude"=>"114.26998436450958"}
		i_name = params[:Iname];
		i_msisdn = params[:Imsisdn];

		title = params[:title];

		day = params[:day];
		month = params[:month];
		year = params[:year];
		date = day.to_s+'/'+month.to_s+'/'+year.to_s;

		hour = params[:hour];
		minute = params[:minute];
		time = hour.to_s+':'+minute.to_s

		count = params[:count].to_i;
		attendees = Hash.new
		# puts title
		# puts day, month, year
		# puts hour, minute
		# puts count
		
		(1..count).each do |i|
			a = 'name'+i.to_s
			b = 'msisdn'+i.to_s
			attendees[a] = params[a]
			attendees[b] = params[b]
			# puts attendees[a]
			# puts attendees[b]
			
		end
		lat = params[:latitude]
		long = params[:longitude]
		# puts lat, long
		code = Meeting.insert_new(i_name, i_msisdn, title, date, time, count+1, lat, long);
		puts code;
		#insert into attendees
		resp = Attend.insert_attendees(code, i_name, i_msisdn, count, attendees);
		puts resp
		if code!=0 and resp==1
			puts 'inside response 1'
			response = {:code => 1}
		else 
			puts 'inside response 2'
			response = {:code => 0}
		end
		render :json => response;
	end

	def meeting_req
		name = params[:name];
		msisdn = params[:msisdn];
		response_hash = Attend.check_meeting_requests(name, msisdn);
		render :json => response_hash;
	end

	# def find_by_id
	# 	name = params[:name];
	# 	msisdn = params[:msisdn];
	# 	response_hash = Attend.check_meeting_requests(name, msisdn);
	# 	render :json => response_hash;
	# end

	def find_meeting_by_id
		id = params[:id]
		response = Meeting.find_meeting(id);
		puts 'got response'
		puts response.inspect
		render :json => response;
	end

	def acc_rej_meeting
		id = params[:id]
		msisdn = params[:msisdn]
		ar = params[:ar]
		begin 
			Attend.where("meeting_id =? AND msisdn=?", id, msisdn).update_all(:accept=>ar)
			response ={:code => 1}
		rescue Exception => exec
			logger.error("ERROR: #{exec.message}")
			response ={:code => 0}
		end
		puts response.inspect
		render :json => response;
	end

	def my_meetings
		name = params[:name];
		msisdn = params[:msisdn];
		response_hash = Attend.check_my_meetings(name, msisdn);
		puts response_hash.inspect
		render :json => response_hash;
	end

	def find_participants
		meeting_id = params[:id];
		resultset = Attend.select(:name, :msisdn, :initiator, :accept).where("meeting_id=? AND cancelled=?", meeting_id,"no");
		hash = {};
		count = 0;
		if !resultset.empty?
			resultset.each do |result|
				count = count+1;
				participant = Hash.new
				participant[:name] = result.name;
				puts result.name;
				participant[:msisdn] = result.msisdn;
				participant[:initiator] = result.initiator;
				participant[:accept] = result.accept;
				resultset2= Location.select(:lat, :long).where("msisdn=?", result.msisdn);
				if !resultset2.empty?
					resultset2.each do |result2|
						participant[:lat]= result2.lat;
						participant[:long]= result2.long;
					end
				else 
					participant[:lat]= "NA";
					participant[:long]= "NA";
				end
				hash[count]=participant;
			end
			hash[:count]= count;
		end
		puts hash.inspect
		render :json => hash;
	end

	def cancel_meeting
		id = params[:id]
		begin 
			Meeting.where("id=?", id).update_all(:cancel=>"yes");
			Attend.where("meeting_id =?", id).update_all(:cancelled=>"yes")
			response ={:code => 1}
		rescue Exception => exec
			logger.error("ERROR: #{exec.message}")
			response ={:code => 0}
		end
		puts response.inspect
		render :json => response;
	end
end