class LocationController < ApplicationController

	def update_location
		name = params[:name]
		msisdn = params[:msisdn]
		lat = params[:lat]
		long = params[:long]
		begin
			# Location.create(:name => name, 
			# 		:msisdn => msisdn, 
			# 		:lat => lat, 
			# 		:long => long);
			Location.where("msisdn=?",msisdn).update_all(:lat=>lat, :long=>long)
			response = {:code => "1"}
			render :json => response
		rescue Exception => exec
			logger.error("ERROR: #{exec.message}")
			response = {:code => "1"}
			render :json => response
		end

	end
end