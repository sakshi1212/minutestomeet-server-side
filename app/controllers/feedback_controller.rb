class FeedbackController < ApplicationController

	def save_feedback
		puts 'starting save feedback - getting parameters'
		msisdn = params[:msisdn];
		name = params[:name];
		feedback = params[:feedback];
		code = Feedback.save_feedback_in_db(name, msisdn, feedback);
		response = {:code => code}
		render :json => response;
	end

end