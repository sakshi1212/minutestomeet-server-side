class CreateAttends < ActiveRecord::Migration
  def change
    create_table :attends do |t|
      t.integer :meeting_id
      t.string :name, limit: 32
      t.string :msisdn, limit: 10
      t.string :initiator, limit: 2
      t.string :accept, limit: 3
      t.string :cancelled, limit: 4

      t.timestamps
    end
  end
end
