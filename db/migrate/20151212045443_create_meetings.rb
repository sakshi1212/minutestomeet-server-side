class CreateMeetings < ActiveRecord::Migration
  def change
    create_table :meetings do |t|
      t.string :title, limit: 32
      t.string :i_name, limit: 32
      t.string :i_msisdn, limit: 10
      t.string :time, limit: 6
      t.string :date, limit: 10
      t.string :loc_lat, limit: 32
      t.string :loc_long, limit: 32
      t.integer :tot_mem

      t.timestamps
    end
  end
end
