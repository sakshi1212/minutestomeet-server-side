class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.string :msisdn, limit: 10
      t.string :name, limit: 32
      t.string :lat, limit: 32
      t.string :long, limit: 32

      t.timestamps
    end
  end
end
