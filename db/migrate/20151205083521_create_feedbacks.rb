class CreateFeedbacks < ActiveRecord::Migration
  def change
    create_table :feedbacks do |t|
      t.string :name, limit: 30
      t.string :msisdn, limit: 10
      t.string :feedback, limit: 128

      t.timestamps
    end
  end
end
