class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :email, limit: 32
      t.string :password, limit: 8
      t.string :name, limit: 16
      t.string :msisdn, limit: 11
      t.string :imei, limit: 32
      t.string :dp_url, limit: 128

      t.timestamps
    end
  end
end
