class AddCancelToMeeting < ActiveRecord::Migration
  def change
    add_column :meetings, :cancel, :string, limit: 4
  end
end
