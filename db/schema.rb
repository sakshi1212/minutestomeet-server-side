# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151212165102) do

  create_table "attends", force: true do |t|
    t.integer  "meeting_id"
    t.string   "name",       limit: 32
    t.string   "msisdn",     limit: 10
    t.string   "initiator",  limit: 2
    t.string   "accept",     limit: 3
    t.string   "cancelled",  limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "feedbacks", force: true do |t|
    t.string   "name",       limit: 30
    t.string   "msisdn",     limit: 10
    t.string   "feedback",   limit: 128
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "locations", force: true do |t|
    t.string   "msisdn",     limit: 10
    t.string   "name",       limit: 32
    t.string   "lat",        limit: 32
    t.string   "long",       limit: 32
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "meetings", force: true do |t|
    t.string   "title",      limit: 32
    t.string   "i_name",     limit: 32
    t.string   "i_msisdn",   limit: 10
    t.string   "time",       limit: 6
    t.string   "date",       limit: 10
    t.string   "loc_lat",    limit: 32
    t.string   "loc_long",   limit: 32
    t.integer  "tot_mem"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "cancel",     limit: 4
  end

  create_table "users", force: true do |t|
    t.string   "email",      limit: 32
    t.string   "password",   limit: 8
    t.string   "name",       limit: 16
    t.string   "msisdn",     limit: 11
    t.string   "imei",       limit: 32
    t.string   "dp_url",     limit: 128
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
