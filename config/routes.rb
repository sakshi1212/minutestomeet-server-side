Rails.application.routes.draw do

  post "login/reqforlogin" => "login#req_for_login"
  post "registrations" => "registrations#signup"
  post "feedback" => "feedback#save_feedback"
  post "meeting/new_meeting" => "meeting#new_meeting"
  post "meeting/meeting_req" => "meeting#meeting_req"
  post "meeting/find_by_id" => "meeting#find_meeting_by_id"
  post "meeting/acc_rej_meeting" => "meeting#acc_rej_meeting"
  post "location/update_location" => "location#update_location"
  post "meeting/my_meetings" => "meeting#my_meetings"
  post "meeting/find_participants" => "meeting#find_participants"
  post "meeting/cancel_meeting" => "meeting#cancel_meeting"

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
 
  #post "registrations/debug"  => "registrations#create_new_for_debugging"
end
